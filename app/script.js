try {
    Chart.defaults.global.animation.duration = 0;
} catch (err) {
}
const DOM = Array.prototype.reduce.call(document.querySelectorAll('[data-ref]'), (acc, node) => {
    acc[node.getAttribute('data-ref')] = node;
    return acc;
}, {});


function main() {
    let calc = new Calculator();
    let state = window.state = new State('r8')
        .linkDomInputs({
            startingDate: DOM.startingDate,
            endDate: DOM.endDate,
            startingAmount: DOM.startingAmount,
            monthlyContribution: DOM.monthlyContribution,
            appartmentCost: DOM.appartmentCost,
            appartmentExpence: DOM.appartmentExpence,
            appartmentYield: DOM.appartmentYield
        })
        .subscribeDomChange(null, () => calc.setParams(state.data.bindings).calc());

    let chart = new Chart(
        DOM.resultGraph.getContext('2d'),
        {
            type: 'line',
            data: {},
            options: {}
        });

    calc.subscribeCalc(result => {
        DOM.outStartingDate.textContent = formatDate(calc.startingDate);
        DOM.outEndDate.textContent = formatDate(calc.endDate);

        let moneyDS = newDS('Money', '#00c700');
        let contributionDS = newDS('Contribution', '#e5a900');
        let yieldDS = newDS('Yield', '#e57202');
        let chartData = {
            labels: [],
            datasets: [contributionDS, yieldDS, moneyDS]
        };

        for (let i = 0; i < result.monthly.length; i++) {
            let values = result.monthly[i];
            chartData.labels.push(formatDate(values.date));
            moneyDS.data.push(Math.floor(values.money));
            contributionDS.data.push(Math.floor(values.contributePerMonth));
            yieldDS.data.push(Math.floor(values.incomePerMonth));
        }

        chart.data = chartData;
        chart.update();

        // Buy Dates
        DOM.resultsList.textContent = '';
        result.buyDates.forEach(data => {
            let line = new El('div').cls('keyv');
            line.cr('div').value(formatDate(data.date));
            line.cr('div').value(formatMoney(data.incomePerMonth));
            DOM.resultsList.appendChild(line.el);
        });

        // Detalization
        DOM.resultsListDetailed.textContent = '';
        let line = new El('div').cls('month_stat');
        let lineContent = line.cr('div').cls('split_view');
        lineContent.cr('div').cls('w4').value('Month');
        lineContent.cr('div').cls('w4').value('Contribution');
        lineContent.cr('div').cls('w4').value('Deals');
        DOM.resultsListDetailed.appendChild(line.el);

        function createEditableMonthStat(data) {
            state.data.monthStats = state.data.monthStats || {};
            let isEditState = !!state.data.monthStats[formatDateTech(data.date)] || false;
            let line = new El('div').cls('month_stat');
            let sw = line
                .cr('input')
                .attr({type: 'checkbox'})
                .ev('click', ev => {
                    isEditState = ev.target.checked;
                    renderEl();
                    calc.calc();
                }); sw.el.checked = isEditState;
            let lineContent = line.cr('div').cls('split_view');

            function renderEl() {
                lineContent.value('');
                if ( isEditState ) {
                    let stateData = state.data.monthStats[formatDateTech(data.date)]
                        = state.data.monthStats[formatDateTech(data.date)] || {};
                    stateData.contributePerMonth = data.contributePerMonth;

                    lineContent.cr('div').cls('w4').value(formatDate(data.date));
                    lineContent.cr('input').cls('w4')
                        .attr({type: 'number'}).value(data.contributePerMonth)
                        .ev('blur', ev => {
                            stateData.contributePerMonth = +ev.target.value;
                            state.data; // save
                            calc.calc();
                        });

                    let addBuyLine = lineContent.cr('div').cls('w4');
                    let swBuy = addBuyLine
                        .cr('input')
                        .attr({type: 'checkbox'})
                        .ev('click', ev => {
                            stateData.isBuying = ev.target.checked;
                            state.data; // save
                            calc.calc();
                            renderEl();
                        }); swBuy.el.checked = stateData.isBuying;
                    addBuyLine.cr('span').value('Deal');

                    if ( stateData.isBuying ) {
                        lineContent.cr('div').cls('w4').value('Available');
                        lineContent.cr('div').cls('w8').value(formatMoney(data.money));
                        lineContent.cr('div').cls('w4').value('Balance');
                        lineContent.cr('input').attr({type: 'text'}).cls('w8')
                            .ev('blur', ev => {
                                stateData.balanceUpdate = +ev.target.value;
                                state.data; // save
                                calc.calc();
                            }).value(stateData.balanceUpdate);
                        lineContent.cr('div').cls('w4').value('Income');
                        lineContent.cr('input').attr({type: 'text'}).cls('w8')
                            .ev('blur', ev => {
                                stateData.incomeUpdate = +ev.target.value;
                                state.data; // save
                                calc.calc();
                            }).value(stateData.incomeUpdate);
                    }
                } else {
                    lineContent.cr('div').cls('w4').value(formatDate(data.date));
                    lineContent.cr('div').cls('w8').value(formatMoney(data.contributePerMonth));
                    delete state.data.monthStats[formatDateTech(data.date)];
                }

            }
            renderEl();

            return line;
        }

        result.monthly.forEach(data => {
            if ( data.date.getMonth() === 0 ) {
                DOM.resultsListDetailed.appendChild(new El('h4').cls('padded').value(data.date.getFullYear()).el);
            }
            let dom = createEditableMonthStat(data);
            if ( data.isBuying ) {
                dom.cls('month_stat buy');
            }
            DOM.resultsListDetailed.appendChild(dom.el);
        });
    });

    calc.setParams(state.data.bindings).calc();
}

main();
