const config = {
    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dateFormat: date => `${date.getFullYear()} ${config.months[date.getMonth()]}`,
    graphStyle: {
        borderWidth: 2,
        backgroundColor: 'transparent',
        pointBorderWidth: 0,
        pointBorderColor: 'transparent',
        // pointBackgroundColor: 'transparent',
        pointRadius: 3,
        lineTension: 0
    }
};
