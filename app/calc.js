
class Calculator {
    constructor(params) {
        this.workers = [];
        if (params) {
            this.setParams(params);
        }
    }

    setParams(params = {}) {
        this.startingDate = new Date(params.startingDate || Date.now());
        this.endDate = new Date(params.endDate || Date.now());
        this.startingAmount = parseFloat(params.startingAmount || 0);
        this.monthlyContribution = parseFloat(params.monthlyContribution || 0);
        this.appartmentCost = parseFloat(params.appartmentCost || 0);
        this.appartmentExpencePerYear = parseFloat(params.appartmentExpence || 0);
        this.appartmentExpence = this.appartmentExpencePerYear / 12;
        this.appartmentYield = parseFloat(params.appartmentYield || 0);
        return this;
    }

    subscribeCalc(worker) {
        this.workers.push(worker);
    }

    calcMonth(monthSummary, result) {
        if ( state.data.monthStats && state.data.monthStats[formatDateTech(monthSummary.date)] ) {
            Object.assign(monthSummary, state.data.monthStats[formatDateTech(monthSummary.date)]);
        } else if ( result.total.money > this.appartmentCost ) {
            monthSummary.isBuying = true;
            monthSummary.balanceUpdate = -this.appartmentCost;
            monthSummary.incomeUpdate = this.appartmentYield - this.appartmentExpence;
            monthSummary.appartments = result.total.appartments += 1;
            monthSummary.actives = result.total.actives += this.appartmentCost;
        }
        monthSummary.incomePerMonth = result.total.incomePerMonth += monthSummary.incomeUpdate;
        result.total.contributePerMonth = this.monthlyContribution + monthSummary.incomePerMonth;
        // monthSummary.contributePerMonth += monthSummary.incomePerMonth;

        monthSummary.money = result.total.money
            = result.total.money + monthSummary.contributePerMonth + monthSummary.balanceUpdate;

        if ( monthSummary.incomeUpdate !== 0 ) {
            result.buyDates.push(monthSummary);
        }
    }

    calc() {
        if (isNaN(this.startingDate) || isNaN(this.endDate)
            || Math.abs(this.startingDate.getFullYear() - this.endDate.getFullYear()) > 100) {
            return this;
        }
        let result = {
            buyDates: [],
            monthly: [],
            total: {
                money: this.startingAmount,
                contributePerMonth: this.monthlyContribution,
                incomePerMonth: 0,
                actives: 0,
                appartments: 0,
                date: this.startingDate
            }
        };

        for ( let dateIterator = new Date(this.startingDate); dateIterator < this.endDate; ) {
            let monthSummary = {
                date: result.total.date = new Date(dateIterator),
                incomePerMonth: result.total.incomePerMonth,
                contributePerMonth: result.total.contributePerMonth,
                actives: result.total.actives,
                appartments: result.total.appartments,
                incomeUpdate: 0,
                balanceUpdate: 0
            };
            this.calcMonth(monthSummary, result);
            result.monthly.push(monthSummary);
            dateIterator.setMonth(dateIterator.getMonth() + 1);
        }

        this.workers.forEach(w => w(result, this));
        return this;
    }
}
