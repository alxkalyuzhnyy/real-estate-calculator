class State {
    constructor(revision = 1) {
        this.revision = revision;
        this._update = debounce(this.setUrl.bind(this), 300);
        this.workers = {};
        this.storageOptimizationKeys =
            ('revision,bindings,startingDate,endDate,startingAmount,monthlyContribution,appartmentCost,appartmentExpence,' +
            'appartmentYield,monthStats,contributePerMonth,true,false,isBuying,balanceUpdate,incomeUpdate,' +
            '0000,1000,2000,3000,4000,5000,6000,7000,8000,9000,000,100,200,500').split(',');
        this.storageOptimizationPostKeys = ['","','":"','":{"','"},"'];
        this.storageOptimizationRetrieveKeys = this.storageOptimizationKeys.concat(this.storageOptimizationPostKeys);
        // this.storageOptimizationRetrieveKeys.length has to be no more then 36 or the encode will break content
        this.readUrl();
    }

    get data() {
        this._update();
        return this._data;
    }

    set data(value) {
        this._update();
        return this._data = value;
    }

    encode(str) {
        return str
            .replace(new RegExp('('+this.storageOptimizationKeys.join('|')+')([^\\w\\d])', 'g'), (match, key, c2) => {
                return `\$${this.storageOptimizationKeys.indexOf(key).toString(36)}${c2}`;
            })
            .replace(new RegExp('('+this.storageOptimizationPostKeys.join('|')+')([^\\w\\d])', 'g'), (match, key, c2) => {
                return `\$${(this.storageOptimizationKeys.length + this.storageOptimizationPostKeys.indexOf(key)).toString(36)}${c2}`;
            }).replace(/(\$[\d\w])+/g, match => {
                return '$'+match.replace(/\$/g,'');
            });
    }
    decode(str) {
        return str.replace(/\$([\d\w]+)/g, (match, n) => {
            let result = '';
            for ( let i = 0; i < n.length; i++ ) {
                result += this.storageOptimizationRetrieveKeys[parseInt(n[i], 36)];
            }
            return result;
        });
    }

    setUrl() {
        let dataString = JSON.stringify(this._data);
        window.location.replace(window.location.href.split('#')[0] + '#' + btoa(this.encode(dataString)));
    }

    readUrl() {
        let dataString = window.location.href.split('#')[1];

        if ( !dataString || !dataString.length ) {
            return this._data = {};
        }
        try {
            let initialLength = dataString.length;
            let normalized = atob(dataString);
            let decoded = this.decode(normalized);
            console.info(`Decompression status:\n\thash size: ${initialLength}\n\
\tencoded data size: ${normalized.length}\n\
\tdecoded data size: ${decoded.length}\n\
\tavailable optimization keys: ${36-this.storageOptimizationRetrieveKeys.length}`);
            this._data = JSON.parse(decoded);
        } catch (err) {
            this._data = {};
        }
        if ( this.revision !== this._data.revision ) {
            this._data = { revision: this.revision };
            this._update();
        }
        return this._data
    }

    linkDomInputs(params) {
        for ( let key in params ) {
            let domEl = params[key];
            this._data.bindings = this._data.bindings || {};
            this._linkDomInput(domEl, key);
        }
        this._update();
        return this;
    }

    _linkDomInput(domEl, key) {
        domEl.value = this._data.bindings[key] || '';
        domEl.addEventListener('keyup', () => {
            this._data.bindings[key] = domEl.value;
            if ( this.workers[key] ) { this.workers[key].forEach(w => w(this._data.bindings[key])); }
            if ( this.workers.global ) { this.workers.global.forEach(w => w(this._data.bindings)); }
            this._update();
        })
    }

    subscribeDomChange(eventName, worker) {
        eventName = eventName || 'global';
        this.workers[eventName] = this.workers[eventName] || [];
        this.workers[eventName].push(worker);
        return this;
    }
}
