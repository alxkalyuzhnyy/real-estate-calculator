function debounce(fn, timer = 1) {
    let ts;

    return function(...args) {
        if ( ts ) {
            clearTimeout(ts);
            ts = null;
        }
        ts = setTimeout(() => fn(...args), timer)
    }
}

function toLength(value, length = 2) {
    value = value.toString();
    return new Array(length - value.length).fill('0').join('') + value;
}

function formatMoney(value, d = '.', t = ',') {
    let str = String(parseInt(value = Math.abs(Number(value) || 0).toFixed(2)));
    let ind = str.length % 3;

    return value < 0 ? "-" : ""
        + (ind ? str.substr(0, ind) + t : "")
        + str.substr(ind).replace(/(\d{3})(?=\d)/g, "$1" + t)
        + d + Math.abs(value - str).toFixed(2).slice(2);
}

function formatDate(value) {
    let date = new Date(value);
    if ( isNaN(date) ) {
        return 'Invalid Date';
    }
    return config.dateFormat(date);
}
function formatDateTech(value) {
    let date = new Date(value);
    return 'a' + date.getFullYear() + date.getMonth();
}

class El {
    constructor(tag, attrs) {
        this.tag = tag;
        this.el = document.createElement(tag);
        if ( attrs ) {
            this.attr(attrs);
        }
    }

    cls(value) {
        if ( value ) {
            this._cls = value;
            this.el.className = value;
        }
        return this;
    }

    attr(value) {
        if ( typeof(value) === 'string') {
            return this.el.getAttribute(value);
        }
        for (let k in value) {
            this.el.setAttribute(k, value[k]);
        }
        return this;
    }

    cr(tag) {
        let newEl = new El(tag);
        this.el.appendChild(newEl.el);
        return newEl;
    }

    value(value) {
        if ( typeof(value) !== 'undefined' ) {
            if ( typeof(this.el.value) !== 'undefined' ) {
                this.el.value = value;
                return this;
            }
            this.el.textContent = value;
            return this;
        }
        return this.el.value || this.el.textContent;
    }

    ev(ev, worker) {
        this.el.addEventListener(ev, worker);
        return this;
    }
}

function newDS(label, color) {
    return Object.assign({
        label,
        data: [],
        borderColor: color,
        pointBackgroundColor: color
    }, config.graphStyle);
}
